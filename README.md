# SIEMPLE

SIEMPLE is a small project that aims to use ansible and bash scripts to automate the deployment of a SIEM solution for SME.

A SIEM (Security Information and Event Manager) is a tool that collects logs from multiple sources and provides a single point of view to detect abnormal behavior on the environment.

This project uses ansible playbooks and bash scripts to deploy the components that make up the SIEM solution.

The main components of the solution are Elasticsearch, Logstash and Kibana (together they form the ELK stack).

Elasticsearch is the main database, it stores all of the information received.
Logstash is the event/log collection tool, it's used to collect the events/logs sent from the various sources/devices.
Kibana is the main dashboard, it allows the user to search the information in the database.
